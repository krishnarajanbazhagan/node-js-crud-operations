const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const db = require('./db');
const authMiddleware = require('./authMiddleware');

const router = express.Router();

// User Registration API
router.post('/register', async (req, res) => {
  const { name, username, bio, age, password } = req.body;

  const hashedPassword = await bcrypt.hash(password, 10);

  const sql = `INSERT INTO users (name, username, bio, age, password) VALUES (?, ?, ?, ?, ?)`;
  const values = [name, username, bio, age, hashedPassword];

  db.query(sql, values, (err, result) => {
    if (err) {
      console.error('Error registering user:', err);
      res.status(500).send('Internal Server Error');
      return;
    }

    res.status(201).send({ message: 'User registration successful', user: { name, username } });
  });
});

// User Login API
router.post('/login', async (req, res) => {
  const { username, password } = req.body;

  const sql = `SELECT * FROM users WHERE username = ?`;
  const values = [username];

  db.query(sql, values, async (err, result) => {
    if (err) {
      console.error('Error logging in user:', err);
      res.status(500).send('Internal Server Error');
      return;
    }

    if (result.length === 0) {
      res.status(401).send('Invalid username or password');
      return;
    }

    const user = result[0];
    const isValidPassword = await bcrypt.compare(password, user.password);

    if (!isValidPassword) {
      res.status(401).send('Invalid username or password');
      return;
    }

    const token = jwt.sign({ userId: user.id }, 'secretKey', { expiresIn: '1h' });

    res.status(200).send({ token, user: { name: user.name, username: user.username } });
  });
});

// Get User Details API
router.get('/details', authMiddleware, async (req, res) => {
  const userId = req.userId;

  const sql = `SELECT * FROM users WHERE id = ?`;
  const values = [userId];

  db.query(sql, values, (err, result) => {
    if (err) {
      console.error('Error getting user details:', err);
      res.status(500).send('Internal Server Error');
      return;
    }

    if (result.length === 0) {
      res.status(404).send('User not found');
      return;
    }

    const user = result[0];
    res.status(200).send({ user: { name: user.name, username: user.username, bio: user.bio, age: user.age } });
  });
});

// Update User Details API
router.put('/update', authMiddleware, async (req, res) => {
  const userId = req.userId;
  const { name, bio, age } = req.body;

  const sql = `UPDATE users SET name = ?, bio = ?, age = ? WHERE id = ?`;
  const values = [name, bio, age, userId];

  db.query(sql, values, (err, result) => {
    if (err) {
      console.error('Error updating user details:', err);
      res.status(500).send('Internal Server Error');
      return;
    }

    if (result.affectedRows === 0) {
      res.status(404).send('User not found');
      return;
    }

    res.status(200).send({ message: 'User details updated successfully' });
  });
});

// Delete User Account API
router.delete('/delete', authMiddleware, async (req, res) => {
  const userId = req.userId;

  const sql = `DELETE FROM users WHERE id = ?`;
  const values = [userId];

  db.query(sql, values, (err, result) => {
    if (err) {
      console.error('Error deleting user account:', err);
      res.status(500).send('Internal Server Error');
      return;
    }

    if (result.affectedRows === 0) {
      res.status(404).send('User not found');
      return;
    }

    res.status(200).send({ message: 'User account deleted successfully' });
  });
});

module.exports = router;
